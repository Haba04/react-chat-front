import React from 'react';
import {Form, Input} from "antd";
import AliwangwangOutlined from "@ant-design/icons/es/icons/AliwangwangOutlined";
import {LockOutlined} from "@ant-design/icons";
import {Link} from "react-router-dom";
import {Button, Block} from "../../../components";

import './LoginForm.scss'

const LoginForm = props => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
    } = props;

    return (
        <>
            <div className='auth__top'>
                <h2>Войти в аккаунт</h2>
                <p>Пожалуйста, войдите в свой аккаунт</p>
            </div>

            <Block>
                <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{remember: true}}
                    onSubmit={handleSubmit}
                >
                    <Form.Item
                        validateStatus={
                            !touched.email ? '' : errors.email ? 'error' : 'success'
                        }
                        help={!touched.email ? '' : errors.email}
                        hasFeedback
                    >
                        <Input
                            id='email'
                            size='large'
                            prefix={<AliwangwangOutlined className="site-form-item-icon"/>}
                            placeholder=" E-Mail"
                            value={values.email}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                    </Form.Item>
                    <Form.Item
                        validateStatus={
                            !touched.password ? '' : errors.password ? 'error' : 'success'
                        }
                        help={!touched.password ? '' : errors.password}
                        hasFeedback
                    >
                        <Input
                            id='password'
                            type='password'
                            size='large'
                            prefix={<LockOutlined className="site-form-item-icon"/>}
                            placeholder=" Пароль"
                            value={values.password}
                            onChange={handleChange}
                            onBlur={handleBlur}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button onClick={handleSubmit} type="primary" size='large'>
                            Войти в аккаунт
                        </Button>
                    </Form.Item>
                    <Link className='auth__register-link' to="/register">Зарегистрироваться</Link>
                </Form>
            </Block>
        </>
    );
};

export default LoginForm;