import React from 'react';
import {Form, Input} from "antd";
import AliwangwangOutlined from "@ant-design/icons/es/icons/AliwangwangOutlined";
import {LockOutlined, UserOutlined} from "@ant-design/icons";
import {Link} from "react-router-dom";
import {Button, Block} from "../../../components";
import InfoCircleTwoTone from "@ant-design/icons/es/icons/InfoCircleTwoTone";

import './RegisterForm.scss'

const RegisterForm = props => {
    const {
        values,
        touched,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        isValid
    } = props;

    const success = false;

    return (
        <>
            <div className='auth__top'>
                <h2>Регистрация</h2>
                <p>Для входа в чат, вам нужно зарегистрироваться</p>
            </div>

            <Block>

                {success
                    ?  <div className='auth__success-block'>
                        <div><InfoCircleTwoTone /></div>
                        <h2>Подтвердите свой аккаунт</h2>
                        <p>На Вашу почту отправлено письмо с ссылкой на <br/>
                            подтверждение аккаунта.</p>
                    </div>

                    : <Form
                        name="normal_login"
                        className="login-form"
                        initialValues={{remember: true}}
                        onSubmit={handleSubmit}
                    >
                        <Form.Item
                            validateStatus={
                                !touched.email ? '' : errors.email ? 'error' : 'success'
                            }
                            help={!touched.email ? '' : errors.email}
                            hasFeedback
                        >
                            <Input
                                id='email'
                                size='large'
                                prefix={<AliwangwangOutlined className="site-form-item-icon"/>}
                                placeholder=" E-Mail"
                                value={values.email}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </Form.Item>
                        <Form.Item
                            // validateStatus='success'
                            hasFeedback
                        >
                            <Input
                                size='large'
                                prefix={<UserOutlined className="site-form-item-icon"/>}
                                placeholder=" Ваше имя"
                            />
                        </Form.Item>
                        <Form.Item
                            validateStatus={
                                !touched.password ? '' : errors.password ? 'error' : 'success'
                            }
                            help={!touched.password ? '' : errors.password}
                            hasFeedback
                        >
                            <Input
                                id='password'
                                type='password'
                                size='large'
                                prefix={<LockOutlined className="site-form-item-icon"/>}
                                placeholder=" Пароль"
                                value={values.password}
                                onChange={handleChange}
                                onBlur={handleBlur}
                            />
                        </Form.Item>
                        <Form.Item
                            // validateStatus='success'
                            hasFeedback
                        >
                            <Input
                                size='large'
                                prefix={<LockOutlined className="site-form-item-icon"/>}
                                placeholder=" Повторить пароль"
                            />
                        </Form.Item>
                        <Form.Item>
                            {isSubmitting && !isValid && <span>Ошибка!</span>}
                            <Button onClick={handleSubmit} type="primary" size='large'>
                                Зарегистрироваться
                            </Button>
                        </Form.Item>
                        <Link className='auth__register-link' to="/login">Войти в аккаунт</Link>
                    </Form>
                }

            </Block>
        </>
    );
};

export default RegisterForm;