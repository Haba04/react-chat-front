export default ({isAuth, errors, values}) => {

    const rules = {
        email: (errors, value) => {
            if (!value) {
                errors.email = 'Введите email';
            } else if (
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
                errors.email = 'Вы ввели некорректный email';
            }
        },

        password: (errors, value) => {
            if (!value) {
                errors.password = 'Введите пароль';
            }  else if (!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test(value)) {
                errors.password = isAuth
                    ? 'Неверный пароль'
                    : 'Должны быть хотябы 1 маленькая буква, 1 заглавная и 1 цифра';
            }
        }
    };

    Object.keys(values).forEach(key => rules[key] && rules[key](errors, values[key]));

    return errors;
};