import tinyColor from 'tinycolor2'

export default (hash) => {
    const [r, g, b] = hash.substr(0, 3);
    return {
        color: tinyColor(`#${r}${g}${b}`).toHexString(),
        colorLight: tinyColor(`#${r}${g}${b}`).lighten(30).toHexString()
    }
}