import React, {useState} from 'react';
import {Input, Button} from "antd";
import {UploadField} from '@navjobs/upload';
import { Picker } from 'emoji-mart';

import './ChatInput.scss';
import {SmileOutlined, CameraOutlined, AudioOutlined, SendOutlined} from "@ant-design/icons/es/icons";

const ChatInput = () => {
    const [value, setValue] = useState('');
    const [emojiPickerVisible, setEmojiPickerVisible] = useState('');

    const toggleEmojiPicker = () => {
        setEmojiPickerVisible(!emojiPickerVisible);
    };

    return (
        <div className='chat-input'>
            <div className="chat-input__smile-btn">
                {emojiPickerVisible && (
                    <div className="chat-input__emoji-picker">
                        <Picker set='apple' />
                    </div>
                )}
                <Button
                    onClick={toggleEmojiPicker}
                    icon={<SmileOutlined/>}
                />
            </div>
            <Input
                size='large'
                placeholder="Введите текст сообщения"
                onChange={e => setValue(e.target.value)}
            />

            <div className="chat-input__actions">

                <UploadField
                    onFiles={files => console.log(files)}
                    containerProps={{
                        className: 'chat-input__actions-upload-btn'
                    }}
                    uploadProps={{
                        accept: '.jpg,.jpeg,.png,.gif,.bmp',
                        multiple: 'multiple'
                    }}
                >
                    <Button icon={<CameraOutlined/>}/>
                </UploadField>

                {value ? <Button icon={<SendOutlined/>}/> : <Button icon={<AudioOutlined/>}/>}
            </div>
        </div>
    );
};

export default ChatInput;