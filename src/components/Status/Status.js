import React from 'react';

import './Status.scss';

const Status = ({online}) => (
    <div className={`status status--${online ? 'online' : 'offline'}`}>
        {online ? 'онлайн' : 'офлайн'}
    </div>
);

export default Status;