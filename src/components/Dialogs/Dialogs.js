import React from 'react';
import orderBy from 'lodash/orderBy'

import './Dialogs.scss';
import {DialogItem} from "../";
import {Input, Empty} from "antd";

const Dialogs = ({filteredItems, userId, onSearch, inputValue, onSelectDialog, currentDialogId}) => (
    <div className="dialogs">
        <div className="chat__sidebar-search">
            <Input.Search
                placeholder="Поиск среди контактов"
                onChange={e => onSearch(e.target.value)}
                value={inputValue}
            />
        </div>

        {filteredItems.length ? (
            orderBy(filteredItems, ["created_at"], 'desc').map(item => (
            <DialogItem
                key={item._id}
                onSelect={onSelectDialog}
                unreaded={item.unreaded}
                isMe={item.user._id === userId}
                currentDialogId={currentDialogId}
                {...item}
            />
        ))
        ) : (
            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description='Ничего не найдено' />
        )}

    </div>
);

export default Dialogs;