import React, {useState, useRef, useEffect} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames'

import convertCurrentTime from '../../util/convertCurrentTime'
import doubleCheckPng from '../../assets/img/doubleCheck.png'
import onceCheckPng from '../../assets/img/onceCheck.png'
import waveSvg from '../../assets/img/wave.svg'
import playSvg from '../../assets/img/play.svg'
import pauseSvg from '../../assets/img/pause.svg'
import './Message.scss';
import {Time} from "../";

const MessageAudio = ({audio}) => {

    const [isPlaying, setIsPlaying] = useState(false);
    const [currentTime, setCurrentTime] = useState(0);
    const [progress, setProgress] = useState(0);
    const audioEl = useRef(null);

    useEffect(() => {
        if (audio) {
            audioEl.current.volume = '.3';
            audioEl.current.addEventListener('playing', () => {
                setIsPlaying(true);
            }, false);

            audioEl.current.addEventListener('ended', () => {
                setIsPlaying(false);
                setProgress(0);
                setCurrentTime(0);
            }, false);

            audioEl.current.addEventListener('pause', () => {
                setIsPlaying(false);
            }, false);

            audioEl.current.addEventListener('timeupdate', () => {
                const duration = (audioEl.current && audioEl.current.duration) || 0;
                setCurrentTime(audioEl.current.currentTime);
                setProgress((audioEl.current.currentTime / duration) * 100);
            });
        }
    }, []);

    const togglePlay = () => {

        if (!isPlaying) {
            audioEl.current.play();
        } else {
            audioEl.current.pause();
        }
    };

    return (
        <div className="message__audio">
            <audio ref={audioEl} src={audio} preload='auto'/>

            <div className="message__audio-progress" style={{width: progress + '%'}}>
            </div>
            <div className="message__audio-info">
                <div className="message__audio-btn">
                    <button onClick={togglePlay}>
                        {isPlaying
                            ? <img src={pauseSvg} alt="pause"/>
                            : <img src={playSvg} alt="play"/>}
                    </button>
                </div>
                <div className="message__audio-wave"><img src={waveSvg} alt="wave svg"/></div>
                <span className="message__audio-duration">
                   {convertCurrentTime(currentTime)}
                </span>
            </div>
        </div>
    )
};

const Message = (data) => {
    const {avatar, user, text, date, audio, isMe, isReaded, attachments, isTyping} = data;

    return (
        <div className={classNames('message', {
            'message--isme': isMe,
            'message--is-typing': isTyping,
            'message--is-audio': audio,
            'message--image': attachments && attachments.length === 1
        })}
        >
            <div className='message__avatar'>
                <img src={avatar} alt={`Avatar ${user.fullName}`}/>
            </div>

            <div className='message__wrap'>
                {isMe && (
                    <img
                        src={isReaded ? doubleCheckPng : onceCheckPng}
                        alt={isReaded ? 'double check icon' : 'once check icon'}
                    />
                )}

                <div className='message__info'>
                    {isTyping
                        ? <div className='message__typing'>
                            <span/>
                            <span/>
                            <span/>
                        </div>
                        : (audio || text) && <div className="message__bubble">

                        {audio && <MessageAudio audio={audio}/>}

                        {!audio && text && <p className='message__text'>{text}</p>}
                    </div>}

                    <div className="message__attachments">
                        {
                            attachments && attachments.map((el, index) => (
                                <div key={index} className="message__attachments-item">
                                    <img src={el.url} alt={el.filename}/>
                                </div>
                            ))
                        }
                    </div>
                    {date && (
                        <span className='message__date'>
                        <Time date={date}/>
                    </span>
                    )}
                </div>
            </div>
        </div>
    );
};

Message.defaultProps = {
    user: {}
};

Message.propTypes = {
    avatar: PropTypes.string,
    text: PropTypes.string,
    user: PropTypes.object,
    attachments: PropTypes.array,
    isTyping: PropTypes.bool,
    audio: PropTypes.string
};

export default Message;