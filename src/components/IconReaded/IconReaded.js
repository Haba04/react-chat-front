import React from 'react';
import doubleCheckPng from "../../assets/img/doubleCheck.png";
import onceCheckPng from "../../assets/img/onceCheck.png";
import PropTypes from "prop-types";

import './IconReaded.scss';

const IconReaded = ({isMe, isReaded}) => (
    isMe &&
    (isReaded ? (
        <img className='message__icon-readed' src={doubleCheckPng} alt='double check icon'/>
    ) : (
        <img className='message__icon-readed--no' src={onceCheckPng} alt='once check icon'/>
    )));

IconReaded.propTypes = {
    isMe: PropTypes.bool,
    isReaded: PropTypes.bool,
};

export default IconReaded;