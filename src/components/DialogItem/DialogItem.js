import React from 'react';
import classNames from 'classnames'
import format from 'date-fns/format'
import isToday from 'date-fns/isToday'

import './DialogItem.scss';
import {IconReaded} from "../index";
import Avatar from '../../components/Avatar/Avatar'

const getMessageTime = create_at => {
    if (isToday(new Date(create_at))) {
        return format(new Date(create_at), 'HH:mm')
    } else
        return format(new Date(create_at), 'dd.MM.yyyy')
};

const DialogItem = ({_id, user, created_at, text, unreaded, isMe, onSelect, currentDialogId}) => (
        <div
            className={classNames('dialogs__item', {
                'dialogs__item--online': user.isOnline,
                'dialogs__item--selected': currentDialogId === _id
            })}
            onClick={onSelect.bind(this, _id)}
        >
            <div className="dialogs__item-avatar">
                <Avatar {...user}/>
            </div>
            <div className="dialogs__item-info">
                <div className="dialogs__item-info-top">
                    <b>{user.fullname}</b>
                    <span>
                        {getMessageTime(created_at)}
                    </span>
                </div>
                <div className="dialogs__item-info-bottom">
                    {text && <p>{text}</p>}

                    {!unreaded && isMe && <IconReaded isMe={true} isReaded={true}/>}

                    {unreaded > 0 && <div className='dialogs__item-info-bottom-count'>{unreaded > 9 ? '+9' : unreaded}</div>}
                </div>
            </div>
        </div>
    );

export default DialogItem;