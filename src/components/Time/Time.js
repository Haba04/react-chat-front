import React from 'react';
import formatDistanceToNow from 'date-fns/formatDistanceToNowStrict';
import ruLocale from 'date-fns/locale/ru'

import './Time.scss';

const Time = ({date}) => (
    <div>
        {formatDistanceToNow(new Date(date), {addSuffix: true, locale: ruLocale})}
    </div>
);

export default Time;