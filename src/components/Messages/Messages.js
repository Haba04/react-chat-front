import React from 'react';
import PropTypes from 'prop-types';
import {Empty} from "antd";
import LoadingOutlined from "@ant-design/icons/es/icons/LoadingOutlined";
import classNames from 'classnames';

import './Messages.scss';
import {Message} from "../index";

const Messages = ({reff, items, isLoading}) => {
    return (
        <div ref={reff} className={classNames('messages', {'messages__loading': isLoading})}>
            {isLoading ? (
                <LoadingOutlined style={{fontSize: 24}} spin/>
            ) : items && !isLoading ? (
                items.length > 0 ? (
                    items.map((item, index) => <Message key={index} {...item}/>)
                ) : (
                    <Empty description='Диалог пуст'/>
                )

            ) : (
                <Empty description='Нет сообщений'/>
            )}
        </div>
    )
};


Messages.propTypes = {
    items: PropTypes.array
};

export default Messages;