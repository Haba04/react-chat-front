import React from 'react';

import './Avatar.scss';
import generateAvatarFromHash from "../../util/generateAvatarFromHash";

const Avatar = ({_id, avatar, fullname}) => {
    if (avatar) {
        return (
            <img className='avatar' src={avatar} alt="user avatar"/>
        );
    } else {
        const {color, colorLight} = generateAvatarFromHash(_id);
        const firstSymbol = fullname.substr(0,1);
        return (
           <div
               style={{background: `linear-gradient(135deg, ${color} 0%, ${colorLight} 96.52%)`}}
               className='avatar avatar--symbol'
           >{firstSymbol.toUpperCase()}</div>
        );
    }
};

export default Avatar;
