import React, {useState, useEffect} from 'react';
import {connect} from "react-redux";

import {Dialogs as BaseDialogs} from "../components";
import {dialogsActions} from '../redux/actions'

const Dialogs = ({fetchDialogs, currentDialogId, setCurrentDialog, dialogList, userId}) => {
    const [inputValue, setInputValue] = useState('');
    const [filteredItems, setFilteredItems] = useState(Array.from(dialogList));

    const onChangeInput = (value) => {
        setFilteredItems(
            dialogList.filter(dialog => dialog.user.fullname.toLowerCase().indexOf(value.toLowerCase()) >= 0)
        );

        setInputValue(value);
    };

    useEffect(() => {
        if (!dialogList.length) {
            fetchDialogs();
        } else {
            setFilteredItems(dialogList);
        }
    }, [dialogList]);

    return (
        <BaseDialogs
            userId={userId}
            filteredItems={filteredItems}
            onSearch={onChangeInput}
            inputValue={inputValue}
            onSelectDialog={setCurrentDialog}
            currentDialogId={currentDialogId}
        />
    );
};

export default connect(
    ({dialogs}) => ({
        dialogList: dialogs.items,
        currentDialogId: dialogs.currentDialogId
    }),
    dialogsActions
)(Dialogs);