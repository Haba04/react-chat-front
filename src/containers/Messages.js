import React, {useEffect, useRef} from 'react';
import {connect} from "react-redux";

import {Messages as Basemessages} from "../components";
import {messagesActions} from '../redux/actions'

const Messages = ({currentDialogId, fetchMessages, messages, isLoading}) => {
    const messagesRef = useRef(null);

    useEffect(() => {
        if (currentDialogId) {
            fetchMessages(currentDialogId);
        }
    }, [currentDialogId]);

    useEffect(() => {
        if (messagesRef.current) {
            messagesRef.current.scrollTo(0, 99999);
        }
    }, [messages]);

    return (
        <Basemessages
            items={messages}
            isLoading={isLoading}
            reff={messagesRef}
        />
    );
};

export default connect(
    ({dialogs, messages}) => ({
        currentDialogId: dialogs.currentDialogId,
        messages: messages.items,
        isLoading: messages.isLoading
    }),
    messagesActions
)(Messages);