import React, {useEffect} from 'react';
import {connect} from "react-redux";

import {Home as HomeComponent} from "../pages";
import {dialogsActions} from '../redux/actions'

const Home = ({fetchDialogs, items, currentDialogId}) => {
    useEffect(() => {
        fetchDialogs();
    }, []);

    const currentItem = () => {
        const keys = Object.keys(items);
        const itemId = keys.filter(key => items[key]._id === currentDialogId);
        return items[itemId];
    };

    return (
        <HomeComponent
            item={currentItem()}
        />
    );
};



export default connect(
    ({dialogs}) => ({
        items: dialogs.items,
        currentDialogId: dialogs.currentDialogId
    }),
    dialogsActions
)(Home);