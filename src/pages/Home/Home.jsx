import React from 'react';
import {TeamOutlined, FormOutlined, EllipsisOutlined} from "@ant-design/icons/es/icons";
import {Button} from "antd";

import './Home.scss'
import {Dialogs, Messages} from "../../containers";
import Status from "../../components/Status/Status";
import ChatInput from "../../components/ChatInput/ChatInput";

const Home = ({item}) => {
    return (
        <section className='home'>
            <div className="chat">
                <div className="chat__sidebar">
                    <div className="chat__sidebar-header">
                        <div>
                            <TeamOutlined style={{fontSize: '20px'}}/>
                            <span>Список диалогов</span>
                        </div>
                        <Button icon={<FormOutlined style={{fontSize: '16px'}}/>}/>
                    </div>

                    <div className="chat__sidebar-dialogs">
                        <Dialogs
                            userId={'8c20398ac9f3ea8b2b0c71d6a84b4da4'}
                        />
                    </div>
                </div>

                <div className="chat__dialog">
                    <div className="chat__dialog-header">
                        <div/>
                        <div className="chat__dialog-header-center">
                            <span className="chat__dialog-header-username">
                                {item && item.user.fullname}
                            </span>
                            <div className="chat__dialog-header-status">
                                <Status online={item && item.user.isOnline}/>
                            </div>
                        </div>
                        <Button icon={<EllipsisOutlined style={{fontSize: '26px'}}/>}/>
                    </div>

                    <div className="chat__dialog-messages">
                        <Messages/>
                    </div>

                    <div className='chat__dialog-input'>
                        <ChatInput/>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Home;